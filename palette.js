const root = document.documentElement;
const style = getComputedStyle(root);

window.sector_data_vis_default_palette = Array.from({ length: 20 }, (o, i) => {
  return style.getPropertyValue(`--chart-palette-color-${i + 1}`).replaceAll(' ', ',')
});
