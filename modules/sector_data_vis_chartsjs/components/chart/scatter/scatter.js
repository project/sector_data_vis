(function ({ sector_charts: settings }, palette, formatters) {

  const charts = settings.scatter;

  Object.keys(charts).forEach(chartId => {
    const chart = charts[chartId];
    const canvas = document.getElementById(chartId);

    if (!canvas) {
      return;
    }

    const labels = chart.data.map(row => row.at(0).trim());

    const data = chart.data.map(row => ({
      x: parseFloat(row.at(1).replace(/,/g, '')),
      y: parseFloat(row.at(2).replace(/,/g, ''))
    }))

    const scatterData = {
      labels,
      datasets: [{
        type: 'scatter',
        borderColor: `rgba(${palette[0]}, 1)`,
        backgroundColor: `rgba(${palette[0]}, .5)`,
        data,
        pointRadius: 10,
        pointHoverRadius: 14
      }]
    }

    let annotations = {};

    Object.keys(chart.intersects).map((axis) => {
      if(chart.intersects[axis]) {
        annotations[`annotation-${axis}`] = {
          type: 'line',
          borderColor: 'var(--annotation-color, black)',
          borderWidth: 1,
          borderDash: [5,5],
          label: {
            backgroundColor: 'transparent',
            color: 'var(--annotation-label-color, black)',
            content: chart.intersects[axis].label,
            display: true
          },
          scaleID: axis,
          value: chart.intersects[axis].value
        }
      }
    });

    new Chart(
      canvas,
      {
        type: chart.type,
        data: scatterData,
        options: {
          responsive: true,
          scales: {
            y: chart.yAxisLabel ? {
              stacked: false,
              title: {
                display: true,
                text: chart.yAxisLabel
              },
              ticks: {
                callback: (value) => {
                  return chart.formatter === 'none' ? value : formatters[chart.formatter].format(value);
                }
              }
            } : null,
            x: chart.xAxisLabel ? {
              stacked: false,
              title: {
                display: true,
                text: chart.xAxisLabel
              }
            } : null,
          },
          plugins: {
            annotation: {
              annotations
            },
            legend: {
              display: false
            },
            tooltip: {
              callbacks: {
                label: (context) => {
                  const value = context.parsed.y;
                  const yValue = chart.formatter === 'none' ? context.parsed.y : formatters[chart.formatter].format(context.parsed.y);
                  const xValue = chart.formatter === 'none' ? context.parsed.x : formatters[chart.formatter].format(context.parsed.x);
                  return `${chart.xAxisLabel}: ${xValue}, ${chart.yAxisLabel}: ${yValue}`;
                }
              }
            }
          }
        }
      }
    );

    // build table

    if (chart.xAxisLabel && chart.yAxisLabel) {
      const table = document.querySelector(`table[aria-describedby="${chartId}"]`)
      const thead = document.createElement('thead');
      const thead__tr = document.createElement('tr');

      const firstColThead = chart.legend.at(0);

      [ firstColThead, chart.xAxisLabel, chart.yAxisLabel ].forEach(heading => {
        const th = document.createElement('th');
        th.textContent = heading;
        thead__tr.appendChild(th)
      })

      thead.appendChild(thead__tr);
      table.appendChild(thead);

      const tbody = document.createElement('tbody');

      chart.data.forEach((row,i) => {
        const tr = document.createElement('tr');
        const th = document.createElement('th');
        th.textContent = row.at(0);

        tr.appendChild(th);
        const values = row.slice(1);

        values.forEach((cell) => {
          const elem = document.createElement('td');
          elem.textContent = chart.formatter === 'none' ? cell : formatters[chart.formatter].format(cell);
          tr.appendChild(elem);
        });

        tbody.appendChild(tr);
      })

      table.appendChild(tbody);

    }

    const table_wrap = canvas.closest('.chart').querySelector('.chart__data-table')
    if (table_wrap) {
      chartTableA11y(table_wrap)
    }

  })




})(drupalSettings, window.sector_data_vis_default_palette, window.sector_data_vis_formatters);
