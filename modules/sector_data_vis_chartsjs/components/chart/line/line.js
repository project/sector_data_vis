(function ({ sector_charts: settings }, palette, formatters) {

  const charts = settings.line;

  Object.keys(charts).forEach(chartId => {
    const chart = charts[chartId];
    const canvas = document.getElementById(chartId);
    if (!canvas) {
      return;
    }

    const xAxisLabels = chart.data.map(dataset => dataset.at(0));

    //const yAxis = chart.data.slice(1).map(row => row.at(0))
    //const xAxis = chart.data.at(0).filter(row => row);
    const dataWithoutLabels = chart.data.map(dataset => dataset.slice(1).map(n => parseFloat(n.replace(/,/g, ''))))

    const numberOfDatasets = dataWithoutLabels.at(0).length

    const datasets = Array.from({ length: dataWithoutLabels.at(0).length }, (n, i) => {
      return {
        label: chart.legend[i],
        data: dataWithoutLabels.map(arr => arr[i]),
        borderColor: `rgba(${palette[i]}, 1)`,
        backgroundColor: `rgba(${palette[i]}, .5)`,
      }
    })

    new Chart(
      canvas,
      {
        type: chart.type,
        data: {
          labels: xAxisLabels,
          datasets
        },
        options: {
          responsive: true,
          scales: {
            y: chart.yAxisLabel ? {
              stacked: false,
              title: {
                display: true,
                text: chart.yAxisLabel
              },
              ticks: {
                callback: (value) => {
                  return chart.formatter === 'none' ? value : formatters[chart.formatter].format(value);
                }
              }
            } : null,
            x: chart.xAxisLabel ? {
              stacked: false,
              title: {
                display: true,
                text: chart.xAxisLabel
              }
            } : null,

          },
          plugins: {
            tooltip: {
              callbacks: {
                label: (context) => {
                  const value = chart.formatter === 'none' ? context.parsed.y : formatters[chart.formatter].format(context.parsed.y);
                  return `${context.dataset.label}: ${value}`
                }
              }
            }
          }
        }
      }
    );

    // build table

    if (xAxisLabels) {
      const table = document.querySelector(`table[aria-describedby="${chartId}"]`)
      const thead = document.createElement('thead');
      const thead__tr = document.createElement('tr');

      [chart.xAxisLabel, ...chart.legend].forEach(heading => {
        const th = document.createElement('th');
        th.textContent = heading;
        thead__tr.appendChild(th)
      })

      thead.appendChild(thead__tr);
      table.appendChild(thead);

      const tbody = document.createElement('tbody');

      xAxisLabels.forEach((row,i) => {
        const tr = document.createElement('tr');
        const th = document.createElement('th');
        th.textContent = row;

        tr.appendChild(th);
        datasets.forEach((column) => {
          const elem = document.createElement('td');
          elem.textContent = chart.formatter === 'none' ? column.data.at(i) : formatters[chart.formatter].format(column.data.at(i));
          tr.appendChild(elem);
        });
        tbody.appendChild(tr);
      })

      table.appendChild(tbody);

    }

    const table_wrap = canvas.closest('.chart').querySelector('.chart__data-table')
    if (table_wrap) {
      chartTableA11y(table_wrap)
    }

  })




})(drupalSettings, window.sector_data_vis_default_palette, window.sector_data_vis_formatters);
