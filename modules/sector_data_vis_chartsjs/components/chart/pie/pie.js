(function ({ sector_charts: settings }, palette, formatters) {
  const charts = settings.pie;

  Object.keys(charts).forEach(chartId => {
    const chart = charts[chartId];
    const canvas = document.getElementById(chartId);
    if (!canvas) {
      return;
    }

    const labels = chart.data.map(row => row.at(0));

    const data = chart.data.map(row => parseFloat(row.at(1).replace(/,/g, '')))

    new Chart(
      canvas,
      {
        type: chart.type,
        data: {
          labels,
          datasets: [ {
            data,
            label: chart.xAxisLabel,
            backgroundColor: data.map((x, i) => {
              return `rgba(${palette[i]}, .5)`
            }),
            hoverOffset: 4
          } ],
        },
        options: {
          responsive: true,
          plugins: {
            tooltip: {
              callbacks: {
                label: (context) => {
                  const value = chart.formatter === 'none' ? context.parsed : formatters[chart.formatter].format(context.parsed);
                  return `${context.dataset.label}: ${value}`
                }
              }
            }
          }
        }
      }
    );

    // build table



    if (chart.legend && chart.xAxisLabel) {
      const table = document.querySelector(`table[aria-describedby="${chartId}"]`)
      const thead = document.createElement('thead');
      const thead__tr = document.createElement('tr');

      [chart.legend, chart.xAxisLabel].forEach(heading => {
        const th = document.createElement('th');
        th.textContent = heading;
        thead__tr.appendChild(th)
      })

      thead.appendChild(thead__tr);
      table.appendChild(thead);

      const tbody = document.createElement('tbody');

      chart.data.forEach((row) => {
        const tr = document.createElement('tr');
        const th = document.createElement('th');
        th.textContent = row.at(0);
        tr.appendChild(th);

        tbody.appendChild(tr);

        const elem = document.createElement('td');
        elem.textContent = chart.formatter === 'none' ? row.at(1) : formatters[chart.formatter].format(row.at(1));
        tr.appendChild(elem);
      })

      table.appendChild(tbody);

    }

    const table_wrap = canvas.closest('.chart').querySelector('.chart__data-table')
    if (table_wrap) {
      chartTableA11y(table_wrap)
    }

  })




})(drupalSettings, window.sector_data_vis_default_palette, window.sector_data_vis_formatters);
