function chartTableA11y(table_wrap) {
    const summary = table_wrap.querySelector('summary');
    const text_elem = summary.querySelector('span:first-child');
    const default_text_elem = text_elem.textContent;
    table_wrap.addEventListener('toggle', e => {
        if (e.newState === 'open') {
          text_elem.textContent = 'Hide data table'
          summary.setAttribute('aria-expanded', true)
        }
        else {
          text_elem.textContent = default_text_elem
          summary.setAttribute('aria-expanded', false)
        }
    })
}