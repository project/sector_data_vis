(function ({ sector_charts: settings }, palette, formatters) {

  const charts = settings.dual;

  const types = [
    'bar',
    'line'
  ]

  Object.keys(charts).forEach(chartId => {
    const chart = charts[chartId];
    const canvas = document.getElementById(chartId);
    if (!canvas) {
      return;
    }

    let datasets = []
    let labels = [];

    chart.data.forEach((dataset, i) => {
      const { headings } = dataset;

      let datagroups = headings.map(heading => ({
        label: heading,
        data: [],
        type: types[i],
        format: chart.formatters[i],
        yAxisID: types[i] === 'bar' ? 'y' : 'z',
        order: types[i] === 'bar' ? 1 : 0,
      }));

      dataset.data.forEach((row, o) => {
        labels = [...labels, row.at(0)];
        const values = row.slice(1).map(z => parseFloat(z));
        values.forEach((column, p) => {
          datagroups[p].data = [...datagroups[p].data, column]
        })
      })

      datagroups.forEach(datagroup => {
        datasets.push(datagroup)
      });
    })

    labels = [...new Set(labels.filter(i => i))];

    const wrapper = canvas.closest('.chart');
    if (wrapper) {
      wrapper.style.setProperty('--chart-cols', labels.length)
      wrapper.style.setProperty('--chart-colwidth', '36px');
    }

    new Chart(
      canvas,
      {
        data: {
          labels,
          datasets: datasets.map((dataset, i) => ({
            ...dataset,
            data: dataset.format === 'percentage' ? dataset.data.map(value => value * 100) : dataset.data,
            borderColor: `rgba(${palette[i]}, 1)`,
            backgroundColor: `rgba(${palette[i]}, .75)`,
          }))
        },
        options: {
          responsive: true,
          scales: {
            y: {
              stacked: false,
              beginAtZero: true,
              position: 'left',
              title: {
                display: chart.yAxisLabel ? true : false,
                text: chart.yAxisLabel
              },
              ticks: {
                callback: (value) => {
                  const formatter = chart.formatters.at(0) ?? NULL;
                  if (formatter === 'none') {
                    return value;
                  }
                  return formatters[chart.formatters[0]].format(value);
                }
              }
            },
            z: {
              stacked: false,
              beginAtZero: true,
              position: 'right',
              title: {
                display: chart.zAxisLabel ? true : false,
                text: chart.zAxisLabel
              },
              ticks: {
                callback: (value) => {
                  const formatter = chart.formatters.at(1) ?? NULL;
                  if (formatter === 'none') {
                    return value;
                  }
                  return formatters[chart.formatters[1]].format(value);
                }
              }
            },
            x: {
              stacked: false,
              title: {
                display: chart.xAxisLabel ? true : false,
                text: chart.xAxisLabel
              }
            },
          },
          plugins: {
            legend: {
              display: false,
            },
            tooltip: {
              callbacks: {
                label: ({ dataset: { label, format, ...props }, ...context }) => {
                  const value = format === 'none' ? context.raw : formatters[format].format(context.raw);
                  if (format === 'percentage') {
                    return `${label}: ${formatters[format].format(context.raw / 100)}`
                  }
                  return `${label}: ${value}`
                }
              }
            }
          }
        }
      }

    );

    // build table
    const table = document.querySelector(`table[aria-describedby="${chartId}"]`)
    const thead = document.createElement('thead');
    const thead__tr1 = document.createElement('tr');
    const thead__tr2 = document.createElement('tr');

    [{
      lbl: '',
      colspan: 1
    }, {
      lbl: chart.yAxisLabel,
      colspan: datasets.filter(({ yAxisID }) => yAxisID === 'y').length
    }, {
      lbl: chart.zAxisLabel,
      colspan: datasets.filter(({ yAxisID }) => yAxisID === 'z').length
    }].forEach((heading, i) => {
      const th = document.createElement('th');
      th.textContent = heading.lbl;
      th.setAttribute('colspan', heading.colspan);
      thead__tr1.appendChild(th);
    });

    ['', ...datasets.map(({ label }) => label),].forEach(heading => {
      const th = document.createElement('th');
      th.textContent = heading;
      thead__tr2.appendChild(th)
    });

    thead.appendChild(thead__tr1);
    thead.appendChild(thead__tr2);
    table.appendChild(thead);

    const tbody = document.createElement('tbody');

    Array.from({ length: labels.length }, (x, o) => {
      const tr = document.createElement('tr');
      const th = document.createElement('th');
      th.textContent = labels[o];
      tr.appendChild(th);

      Array.from({ length: datasets.length }, (z, i) => {
        const td = document.createElement('td');
        const dataset = datasets[i];
        td.textContent = dataset.format !== 'none' ? formatters[dataset.format].format(dataset.data[o]) : dataset.data[o]
        tr.appendChild(td);
      })

      tbody.appendChild(tr);
    },);

    table.appendChild(tbody);

    const table_wrap = canvas.closest('.chart').querySelector('.chart__data-table')
    if (table_wrap) {
      chartTableA11y(table_wrap)
    }

  })

})(drupalSettings, window.sector_data_vis_default_palette, window.sector_data_vis_formatters);
