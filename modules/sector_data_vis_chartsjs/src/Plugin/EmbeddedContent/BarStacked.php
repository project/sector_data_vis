<?php

namespace Drupal\sector_data_vis_chartsjs\Plugin\EmbeddedContent;

use Drupal\embedded_content\EmbeddedContentInterface;
use Drupal\embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\Media;
use Drupal\Component\Utility\Html;

/**
 * Plugin sector_data_vis_chartsjs_bar_stacked.
 *
 * @EmbeddedContent(
 *   id = "sector_data_vis_chartsjs_bar_stacked",
 *   label = @Translation("Data Vis › Chart.js › Stacked Bar"),
 *   description = @Translation("Renders a Sector data visualisation bar chart."),
 * )
 */
class BarStacked extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => NULL,
      'type' => 'bar_stacked',
      'data' => NULL,
      'title' => NULL,
      'first_row_headings' => NULL,
      'xAxisLabel' => NULL,
      'yAxisLabel' => NULL,
      'footnotes' => NULL
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $data       = [];
    $id         = NULL;
    $title      = NULL;
    $sources    = NULL;
    $download   = NULL;
    $headings   = NULL;
    $caption    = NULL;
    $extension  = NULL;

    if (isset($this->configuration['data'])) {
      $media_entity = Media::load($this->configuration['data']);

      if ($media_entity) {
        $field = $media_entity->get('field_media_file')->entity;
        $title = $media_entity->get('name')->value;
        $caption = $media_entity->get('field_caption')->value;

        $id = Html::getId($title);
        if ($field) {
          $file_uri = $field->getFileUri();
          $file_path = \Drupal::service('file_system')->realpath($file_uri);
          $file_info = pathinfo($file_path);
          $extension = $file_info['extension'];
          $download = $field->createFileUrl();
          $size = format_size(filesize($file_path));

          switch ($extension) {
            case 'csv' :
              $csv_data = trim(file_get_contents($file_uri));
              $data = array_map('str_getcsv', explode("\n", trim($csv_data, "\xEF\xBB\xBF")));
              break;
            case 'xlsx':
              $data = [];
              $spreadsheet = IOFactory::load($file_path);
              $worksheet = $spreadsheet->getActiveSheet();

              $i = 0;
              foreach ($worksheet->getRowIterator() as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
                foreach ($cellIterator as $cell) {
                  if (!is_null($cell)) {
                    $data[$i][] = $cell->getValue();
                  }
                }
                $i++;
              }
              break;
          }

          if ($this->configuration['meta']['first_row_headings'] === 1) {
            $headings = $data[0];
            array_shift($headings);
            array_shift($data);
          }
        }

        $sources_field = $media_entity->get('field_sources');
        if (isset($sources_field->value)) {
          $sources = t($sources_field->value);
        }
      }
    }

    $_type = 'bar_stacked';
    $rotate = $this->configuration['rotate'] === 1;
    $rotate_axis = $rotate ? 'y' : 'x';

    $_unique_id = $id . '__' . $_type . '__' . $rotate_axis;    // in case user adds two different interpretations of one dataset on a single page.
    $chart_title = strlen($this->configuration['title'] > 0) ? $this->configuration['title'] : $title;

    return [
      '#theme' => 'chart',
      '#data' => [
        'id' => $_unique_id,
        'type' => $_type,
        'data' => $data,
        'title' => $chart_title,
        'sources' => $sources,
        'download' => $download,
        'caption' => $caption,
        'footnotes' => $this->configuration['footnotes']['value'] ?? NULL,
        'ext' => $extension,
        'size' => $size,
      ],
      '#attached' => [
        'drupalSettings' => [
          'sector_charts' => [
            "$_type" => [
              "$_unique_id" => [
                'id' => $_unique_id,
                'type' => $_type,
                'data' => $data,
                'legend' => $headings,
                'rotate' => $rotate,
                'formatter' => $this->configuration['meta']['formatter'] ?? 'none',
                'title' => $chart_title,
                'xAxisLabel' => $this->configuration['meta']['xAxisLabel'] ?? NULL,
                'yAxisLabel' => $this->configuration['meta']['yAxisLabel'] ?? NULL
              ]
            ]
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['data'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose dataset'),
      '#default_value' => $this->configuration['data'] ? Media::load($this->configuration['data']) : NULL,
      '#target_type' => 'media',
      '#required' => TRUE,
      '#selection_settings' => ['target_bundles' => ['sector_dataset']],
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
      '#description' => t('Leave this empty to use the Dataset name'),
    ];

    $form['rotate'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Horizontal bars'),
        '#default_value' => $this->configuration['rotate'],
        '#description' => t('Rotate stacked bars so the bars are horizontal.'),
    ];

    $form['meta'] = array(
      '#type' => 'details',
      '#title' => t('Chart settings'),
      '#description' => t('This is the main fieldset.'),
      '#open' => FALSE
    );

    $form['meta']['formatter'] = [
      '#type' => 'select',
      '#options' => [
        'none' => 'None',
        'numeric' => 'Numeric',
        'percentage' => '%',
        'currency' => 'Currency'
      ],
      '#title' => $this->t('Choose a formatter?'),
      '#default_value' => $this->configuration['meta']['formatter']
    ];

    $form['meta']['first_row_headings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use first row as headings'),
      '#default_value' => $this->configuration['meta']['first_row_headings'],
      '#description' => t('Use your csv/xlsx first row to render the x and y labels.'),
    ];

    $form['meta']['xAxisLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Horizontal axis label (x)'),
      '#default_value' => $this->configuration['meta']['xAxisLabel']
    ];
    $form['meta']['yAxisLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vertical axis label (y)'),
      '#default_value' => $this->configuration['meta']['yAxisLabel']
    ];

    $form['footnotes'] = [
      '#type' => 'text_format',
      '#allowed_formats' => ['sector_restricted_basic_html'],
      '#title' => $this->t('Caption and footnotes'),
      '#description' => $this->t('A place to add caption and/or footnote content that pertains to this visualisation of the selected dataset media.'),
      '#default_value' => $this->configuration['footnotes']['value'] ?? NULL,
      '#rows' => 3,
    ];

    return $form;
  }

}
