<?php

namespace Drupal\sector_data_vis_chartsjs\Plugin\EmbeddedContent;

use Drupal\embedded_content\EmbeddedContentInterface;
use Drupal\embedded_content\EmbeddedContentPluginBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\media\Entity\Media;
use Drupal\Component\Utility\Html;

/**
 * Plugin sector_data_vis_chartsjs_dual.
 *
 * @EmbeddedContent(
 *   id = "sector_data_vis_chartsjs_dual",
 *   label = @Translation("Data Vis › Chart.js › Dual"),
 *   description = @Translation("Renders a Sector data visualisation dual chart."),
 * )
 */
class Dual extends EmbeddedContentPluginBase implements EmbeddedContentInterface {

  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'id' => NULL,
      'type' => 'line',
      'dataset_1' => [
        'set' => NULL,
        'formatter' => NULL,
      ],
      'dataset_2' => [
        'set' => NULL,
        'formatter' => NULL,
      ],
      'title' => NULL,
      'first_row_headings' => NULL,
      'xAxisLabel' => NULL,
      'yAxisLabel' => NULL,
      'zAxisLabel' => NULL,
      'footnotes' => NULL,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $dataset_1  = [];
    $dataset_2  = [];
    $headings   = NULL;

    $dataset_1 = $this->processDataset($this->configuration['dataset_1']);

    $dataset_2 = $this->processDataset($this->configuration['dataset_2']);

    $_type = 'dual';

    $_unique_id = Html::getId($this->configuration['title']) . '__' . $_type;    // in case user adds two different interpretations of one dataset on a single page.
    $chart_title = $this->configuration['title'];

    return [
      '#theme' => 'chart',
      '#data' => [
        'id' => $_unique_id,
        'type' => $_type,
        'data' => [],
        'title' => $chart_title,
        'sources' => [
          $dataset_1['sources'],
          $dataset_2['sources'],
        ],
        'download' => [
          [
            'src' => $dataset_1['download'],
            'ext' => $dataset_1['extension'],
            'size' => $dataset_1['size'],
          ],
          [
            'src' => $dataset_2['download'],
            'ext' => $dataset_2['extension'],
            'size' => $dataset_2['size'],
          ],
        ],
        'footnotes' => $this->configuration['footnotes']['value'] ?? NULL,
      ],
      '#attached' => [
        'drupalSettings' => [
          'sector_charts' => [
            "$_type" => [
              "$_unique_id" => [
                'id' => $_unique_id,
                'type' => $_type,
                'data' => [
                  $dataset_1,
                  $dataset_2,
                ],
                'formatters' => [
                  $this->configuration['dataset_1']['formatter'],
                  $this->configuration['dataset_2']['formatter'],
                ],
                'legend' => $headings,
                'title' => $chart_title,
                'xAxisLabel' => $this->configuration['meta']['xAxisLabel'] ?? NULL,
                'yAxisLabel' => $this->configuration['meta']['yAxisLabel'] ?? NULL,
                'zAxisLabel' => $this->configuration['meta']['zAxisLabel'] ?? NULL,
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {

    $form['dataset_1'] = [
      '#type' => 'details',
      '#title' => $this->t('Dataset 1 (Bar chart)'),
      '#open' => TRUE,
    ];
    $form['dataset_1']['set'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose dataset'),
      '#default_value' => $this->configuration['dataset_1']['set'] ? Media::load($this->configuration['dataset_1']['set']) : NULL,
      '#target_type' => 'media',
      '#required' => TRUE,
      '#selection_settings' => ['target_bundles' => ['sector_dataset']],
    ];
    $form['dataset_1']['formatter'] = [
      '#type' => 'select',
      '#options' => [
        'none' => $this->t('None'),
        'numeric' => $this->t('Numeric'),
        'percentage' => $this->t('%'),
        'currency' => $this->t('Currency'),
      ],
      '#title' => $this->t('Choose a formatter?'),
      '#default_value' => $this->configuration['dataset_1']['formatter'],
    ];

    $form['dataset_2'] = [
      '#type' => 'details',
      '#title' => $this->t('Dataset 2 (Line chart'),
      '#open' => TRUE,
    ];
    $form['dataset_2']['set'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('Choose dataset'),
      '#default_value' => $this->configuration['dataset_2']['set'] ? Media::load($this->configuration['dataset_2']['set']) : NULL,
      '#target_type' => 'media',
      '#required' => TRUE,
      '#selection_settings' => ['target_bundles' => ['sector_dataset']],
    ];
    $form['dataset_2']['formatter'] = [
      '#type' => 'select',
      '#options' => [
        'none' => $this->t('None'),
        'numeric' => $this->t('Numeric'),
        'percentage' => $this->t('%'),
        'currency' => $this->t('Currency'),
      ],
      '#title' => $this->t('Choose a formatter?'),
      '#default_value' => $this->configuration['dataset_2']['formatter'],
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->configuration['title'],
      '#required' => TRUE,
    ];

    $form['meta'] = array(
      '#type' => 'details',
      '#title' => $this->t('Chart settings'),
      '#description' => $this->t('This is the main fieldset.'),
      '#open' => FALSE
    );

    $form['meta']['first_row_headings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Use first row as headings'),
      '#default_value' => $this->configuration['meta']['first_row_headings'],
      '#description' => $this->t('Use your csv/xlsx first row to render the x and y labels.'),
    ];

    $form['meta']['xAxisLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Horizontal axis label (x)'),
      '#default_value' => $this->configuration['meta']['xAxisLabel']
    ];
    $form['meta']['yAxisLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vertical axis label (y)'),
      '#default_value' => $this->configuration['meta']['yAxisLabel']
    ];
    $form['meta']['zAxisLabel'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Vertical axis label (z)'),
      '#default_value' => $this->configuration['meta']['zAxisLabel']
    ];

    $form['footnotes'] = [
      '#type' => 'text_format',
      '#allowed_formats' => ['sector_restricted_basic_html'],
      '#title' => $this->t('Caption and footnotes'),
      '#description' => $this->t('A place to add caption and/or footnote content that pertains to this visualisation of the selected dataset media.'),
      '#default_value' => $this->configuration['footnotes']['value'] ?? NULL,
      '#rows' => 3,
    ];

    return $form;
  }


  /**
   * {@inheritdoc}
   */
  private function processDataset($dataset) {
    $data = [
      'data' => [],
      'sources' => NULL,
    ];
    $media_entity = Media::load($dataset['set']);

    if (!$media_entity) {
      return [];
    }

    $field = $media_entity->get('field_media_file')->entity;
    $data['title'] = $media_entity->get('name')->value;
    $data['caption'] = $media_entity->get('field_caption')->value;

    $data['id'] = Html::getId($data['title']);
    if ($field) {
      $file_uri = $field->getFileUri();
      $file_path = \Drupal::service('file_system')->realpath($file_uri);
      $file_info = pathinfo($file_path);
      $data['extension'] = $file_info['extension'];

      $data['download'] = $field->createFileUrl();
      $data['size'] = format_size(filesize($file_path));

      switch ($data['extension']) {
        case 'csv':
          $csv_data = trim(file_get_contents($file_uri));
          $data['data'] = array_map('str_getcsv', explode("\n", trim($csv_data, "\xEF\xBB\xBF")));
          break;

        case 'xlsx':
          $data['data'] = [];
          $spreadsheet = IOFactory::load($file_path);
          $worksheet = $spreadsheet->getActiveSheet();

          $i = 0;
          foreach ($worksheet->getRowIterator() as $row) {
            $cellIterator = $row->getCellIterator();
            $cellIterator->setIterateOnlyExistingCells(false); // Loop all cells, even if it is not set
            foreach ($cellIterator as $cell) {
              if (!is_null($cell)) {
                $data['data'][$i][] = $cell->getValue();
              }
            }
            $i++;
          }
          break;
      }

      if ($this->configuration['meta']['first_row_headings'] === 1 && !empty($data['data'])) {
        $data['headings'] = $data['data'][0];
        array_shift($data['headings']);
        array_shift($data['data']);
      }

      $sources_field = $media_entity->get('field_sources');
      if (isset($sources_field->value)) {
        $data['sources'] = $sources_field->value;
      }
    }

    return $data;
  }

}
