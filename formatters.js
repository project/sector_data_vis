window.sector_data_vis_formatters = {
  numeric: new Intl.NumberFormat('en-NZ', { maximumSignificantDigits: 3 }),
  percentage: new Intl.NumberFormat('en-NZ', {
    style: 'percent',
    minimumFractionDigits: 0,
    maximumFractionDigits: 2,
  }),
  currency: new Intl.NumberFormat('en-NZ', { style: 'currency', currency: 'NZD' }),
};
